# Galerie-Fahrenheit website development environment


## Architecture

This website is built with node.js and parcel.

Framework: [Vue.js](https://fr.vuejs.org)

Done with Vue Cli: 

> Customize configuration
> See [Configuration Reference](https://cli.vuejs.org/config/).

### Dependencies

- [Nodejs](https://nodejs.org/en/)
- [imagemagick](https://imagemagick.org/)

### Components

- [Vue-Carousel](https://github.com/SSENSE/vue-carousel#usage)
- [Vue-Modal](https://github.com/euvl/vue-js-modal)
- [Portal-Vue](https://github.com/LinusBorg/portal-vue)


## Development

### Local deployment with docker 

Make configuration offer a simple way to work locally (requires docker and docker compose installed).
Run those commands to deploy GF locally : `make local-deploy`
Then you should access the website from [http://localhost:8080](http://localhost:8080)

> :point_up: If anything looks strange, you can reset your whole environment with `make reset-all` command.
>
> :warning: Be carefull this command will remove all your docker environment as well.

### Working without docker (usefull for CI)

**Todo**

### Useful documentation

- [Gitlab CI tests](https://blog.stephane-robert.info/post/gitlab-valider-ci-yml/)

## Build

### `make gfar-install`
> Install all the dependencies through NPM

### `make gfar-build`
> will install all the dependencies with `npm` and build with parcel into the `dist/` folder.


## Documentation

**Todo** : to test / review

### `make docs-build`
> Build the docs in the `docs` folder/ ready to be served

### `make docs-serve`
> Serve the docs and start a watch script with `nodemon`


## [ Gitlab Token ](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) 

You can create personal access tokens from your GitLab profile.

 1) Log in to GitLab.
 2) In the upper-right corner, click your avatar and select Settings.
 3) On the User Settings menu, select Access Tokens.
 4) Choose a name and optional expiry date for the token.
 5) Choose the desired scopes (api, write and read_repository needed)
 6) Click the Create personal access token button.
 7) Save the personal access token somewhere safe. Once you leave or refresh the page, you won’t be able to access it again



## Todo

DevOps
- [x] problem de versionning de `public`
- [/] problem d'install initial => 403 si build après que le container (fixed in `local-deploy` command)
- [ ] maj de la doc
- [ ] review / fix `gfar-serve-*` and `gfar-docs-*` usages
- [x] gitlabCI
    - Utilisation d'un script pour l'install de l'image docker
    - Docs :
        - https://docs.gitlab.com/ee/ci/yaml/index.html
        - https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
        - https://docs.gitlab.com/ee/ci/quick_start/
        - https://blog.nimbleways.com/let-s-make-faster-gitlab-ci-cd-pipelines/
- [ ] upgrade node version

