#!/bin/bash
# changes the way filesystem is handled to follow
# symbolic links
shopt -s nullglob


CURRENT_DIR=$(pwd)
WATCH="${1:-build}"
SRC_DIR="${2:-src/}"
DIST_DIR="${3:-dist/}"
DOCS_DIR="${4:-docs/}"

FULL_SRC_PATH="${CURRENT_DIR}/${SRC_DIR}"
FULL_DIST_PATH="${CURRENT_DIR}/${DIST_DIR}"
FULL_DOCS_PATH="${CURRENT_DIR}/${DOCS_DIR}"

echo "Starting Build of docs: "
echo "SRC_DIR: ${FULL_SRC_PATH}"
echo "DIST_DIR: ${FULL_DIST_PATH}"
echo "DOCS_DIR: ${FULL_DOCS_PATH}"

DOCS_VARIABLE_FILE_PATH=${FULL_DOCS_PATH}variables.json
PACKAGE_FILE_PATH=${CURRENT_DIR}/package.json
packageFile=$(< $PACKAGE_FILE_PATH)

create_variables() {
  rm -f $DOCS_VARIABLE_FILE_PATH
  touch "$DOCS_VARIABLE_FILE_PATH"
  echo "{" >> "$DOCS_VARIABLE_FILE_PATH"
  echo "  \"package\": $packageFile" >> "$DOCS_VARIABLE_FILE_PATH"
  echo "}" >> "$DOCS_VARIABLE_FILE_PATH"
}

create_documentation() {
  SRC=$1
  DIST=$2
  FILENAME=$3
  COMPONENT_NAME=$4
  DOC_NAME=${FILENAME/.md/}

  mkdir -p $DIST

  cp $SRC $DIST/$FILENAME

  # get line number of resource
  lineNumber="$(grep -n "* ${RESOURCE}" "${FULL_DOCS_PATH}_navbar.md" | tail -n -1 | cut -d: -f1)"

  # replace at line if other category is there
  if [[ $lineNumber != '' ]];then
    replacement="\  * [$DOC_NAME](/$RESOURCE/$DOC_NAME/$DOC_NAME)"
    sed -i "$((lineNumber + 1))i${replacement}" ${FULL_DOCS_PATH}_navbar.md
  else
    # else write at the end
    echo "* $RESOURCE" >> ${FULL_DOCS_PATH}_navbar.md
    echo "  * [$DOC_NAME](/$RESOURCE/$DOC_NAME/$DOC_NAME)" >> ${FULL_DOCS_PATH}_navbar.md
  fi
}

fileArray=($(find $FULL_SRC_PATH -type f -name '*.md'))
# get length of an array
tLen=${#fileArray[@]}

rm ${FULL_DOCS_PATH}_navbar.md
touch ${FULL_DOCS_PATH}_navbar.md

# use for loop read all filenames
for (( i=0; i<${tLen}; i++ ));
do
  FILE_PATH="${fileArray[$i]}"
  # splice the PATH until last slash
  FILE="${FILE_PATH##*/}"
  RESOURCE="${FILE_PATH%*/*}"
  RESOURCE="${RESOURCE##*/}"
  # splice the FILE until .
  FILENAME="${FILE%*.*}"
  # ${FILENAME^} make the first caracter uppercase
  DEST="${FULL_DOCS_PATH}${RESOURCE}/${FILENAME}"
  create_documentation $FILE_PATH $DEST $FILE ${FILENAME^}
done

cp $CURRENT_DIR/README.md $DOCS_DIR/README.md
create_variables
