#!/bin/bash

# from root folder
# example: ./scripts/clean-raws.sh ./src/assets/images/
# this will produce delete all raws image from all folders

# arguments
# $1: path to explore

imgRaw_list=$(find $1 -name "raw.*")

for imgRaw in $imgRaw_list; do

  imgRaw_dir=$(dirname $imgRaw)/
  rm $imgRaw
done
