#!/bin/bash

# from root folder
# example: ./scripts/resize-images.sh ./src/assets/images/
# this will produce a `web.jpg` and `thumbnail.jpg` file for each raw.* image found

# arguments
# $1: path to explore

function resizeWeb() {
  web_file=$2web.jpg
  if test -f "$web_file"; then
    echo "$web_file already exists"
  else
    mogrify -write $web_file -filter Triangle -define filter:support=2 -thumbnail 1920 -unsharp 0.25x0.08+8.3+0.045 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -auto-orient -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB $1
  fi
}

function resizeThumbnail() {
  thumbnail_file=$2thumbnail.jpg
  if test -f "$thumbnail_file"; then
    echo "$thumbnail_file already exists"
  else
    mogrify -write $thumbnail_file -filter Triangle -define filter:support=2 -thumbnail 300 -unsharp 0.25x0.08+8.3+0.045 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -auto-orient -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB $1
  fi
}

imgRaw_list=$(find $1 -name "raw.*")

for imgRaw in $imgRaw_list; do

  imgRaw_dir=$(dirname $imgRaw)/
#  echo $imgRaw
#  echo $imgRaw_dir

  resizeWeb $imgRaw $imgRaw_dir
  resizeThumbnail $imgRaw $imgRaw_dir

done
