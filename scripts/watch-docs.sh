#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

npx nodemon -e md --watch ./ --ignore 'docs/' --exec "${SCRIPT_DIR}/build-docs.sh" &
npm run docs:serve 
