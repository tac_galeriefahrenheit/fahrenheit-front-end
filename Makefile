# Context variables
####################

# Useful to ensure files produced in volumes over docker-compose exec
# commands are not "root privileged" files.
export HOST_UID=$(shell id -u)
export HOST_GID=$(shell id -g)
export HOST_ID=${HOST_UID}:${HOST_GID}

# PROJECT_NAME defaults to name of the current directory.
# should not be changed if you follow GitOps operating procedures.
export PROJECT_NAME = $(notdir $(PWD))

export PROJECT_ARTIFACTS_PATH = $(shell realpath ./public)

# export CURRENT_DATE = $(shell date +"%Y%m%d")

export INFRA_SCRIPT_PATH = $(shell realpath ./scripts)
export INFRA_GFAR_SRC_PATH = $(shell realpath ./src/galerie-fahrenheit)
export INFRA_DOCKER_PATH = $(shell realpath ./infra/docker)
export INFRA_CACHE_PATH = $(shell realpath ./infra/cache)

export GFAR_ENV_SRC_PATH ?= /usr/src/galerie-fahrenheit
export GFAR_ENV_SCRIPT_PATH ?= /usr/src/scripts

DOCKER_COMPOSE ?= docker compose \
    -f ${INFRA_DOCKER_PATH}/docker-compose.yaml

EXEC_GFAR_CLI ?= ${DOCKER_COMPOSE} exec -u ${HOST_UID}:${HOST_GID} -ti gfar.cli sh -c


## Meta commands used for internal purpose
############################################

# This guard meta command allows to check if environment variable is defined 
guard-%:
	@if [ -z '${${*}}' ]; then echo 'ERROR: variable $* not set' && exit 1; fi


## galerie-fahrenheit service
##############################

gfar-all: gfar-install gfar-build 

gfar-start: gfar-install guard-EXEC_GFAR_CLI
	${EXEC_GFAR_CLI} 'npm start'

gfar-serve-web: gfar-copy-images guard-EXEC_GFAR_CLI
	${EXEC_GFAR_CLI} 'npm start'

gfar-serve-prod: gfar-build guard-EXEC_GFAR_CLI
	${EXEC_GFAR_CLI} 'npm run serve-prod'

gfar-images: guard-EXEC_GFAR_CLI
	${EXEC_GFAR_CLI} 'npm run images'

gfar-build: gfar-install gfar-copy-images gfar-clean-raws guard-EXEC_GFAR_CLI
	${EXEC_GFAR_CLI} 'npm run build'

gfar-docs-build: gfar-install guard-EXEC_GFAR_CLI guard-GFAR_ENV_SCRIPT_PATH
	${EXEC_GFAR_CLI} '${GFAR_ENV_SCRIPT_PATH}/build-docs.sh'

gfar-docs-serve: gfar-install guard-EXEC_GFAR_CLI guard-GFAR_ENV_SCRIPT_PATH
	${EXEC_GFAR_CLI} '/${GFAR_ENV_SCRIPT_PATH}/watch-docs.sh'
	
gfar-install: gfar-img-build guard-EXEC_GFAR_CLI
	${EXEC_GFAR_CLI} 'npm install'

# Todo : check public directory usage
gfar-copy-images: guard-INFRA_GFAR_SRC_PATH
	rm -rf ${INFRA_GFAR_SRC_PATH}/public/assets/images/products/
	mkdir -p ${INFRA_GFAR_SRC_PATH}/public/assets/images/products/
	cp -r ${INFRA_GFAR_SRC_PATH}/src/resources/products/images/* ${INFRA_GFAR_SRC_PATH}/public/assets/images/products/
	cp -r ${INFRA_GFAR_SRC_PATH}/src/assets/images/* ${INFRA_GFAR_SRC_PATH}/public/assets/images/

# rm -rf ${PROJECT_ARTIFACTS_PATH}/assets/images/products/
# mkdir -p ${PROJECT_ARTIFACTS_PATH}/assets/images/products/
# cp -r ${INFRA_GFAR_SRC_PATH}/src/resources/products/images/* ${PROJECT_ARTIFACTS_PATH}/assets/images/products/
# cp -r ${INFRA_GFAR_SRC_PATH}/src/assets/images/* ${PROJECT_ARTIFACTS_PATH}/assets/images/


gfar-img-build: guard-EXEC_GFAR_CLI guard-GFAR_ENV_SCRIPT_PATH guard-GFAR_ENV_SRC_PATH
	${EXEC_GFAR_CLI} '${GFAR_ENV_SCRIPT_PATH}/resize-images.sh ${GFAR_ENV_SRC_PATH}/src/resources/products/images'

gfar-clean-raws: guard-EXEC_GFAR_CLI guard-GFAR_ENV_SCRIPT_PATH guard-GFAR_ENV_SRC_PATH
	${EXEC_GFAR_CLI} '${GFAR_ENV_SCRIPT_PATH}/clean-raws.sh ${GFAR_ENV_SRC_PATH}/public/assets/images/products'

gfar-reset: guard-INFRA_GFAR_SRC_PATH
# 	reset docs
	- rm -rf ${INFRA_GFAR_SRC_PATH}/docs/architecture
	- rm -rf ${INFRA_GFAR_SRC_PATH}/docs/pages
	- rm -f ${INFRA_GFAR_SRC_PATH}/docs/_navbar.md
	- rm -f ${INFRA_GFAR_SRC_PATH}/docs/README.md
	- rm -f ${INFRA_GFAR_SRC_PATH}/docs/variables.json
# 	reset images
	- find ${INFRA_GFAR_SRC_PATH}/src/resources/products/images -name 'web.jpg' -delete
	- find ${INFRA_GFAR_SRC_PATH}/src/resources/products/images -name 'thumbnail.jpg' -delete
# 	reset dist
	- rm -rf ${INFRA_GFAR_SRC_PATH}/dist/

gfar-clean: gfar-reset
	- rm -rf ${INFRA_GFAR_SRC_PATH}/node_modules/
	- rm -rf ${INFRA_GFAR_SRC_PATH}/.cache/


## Docker commands
## (used for deployment tests)
###############################

# DOCKER-COMPOSE := docker-compose -f infra/docker/gitlab-pages_docker-compose.yaml

docker-start: guard-DOCKER_COMPOSE
	${DOCKER_COMPOSE} up -d --remove-orphans --build

docker-stop:
	${DOCKER_COMPOSE} stop

docker-shell-nginx:
	${DOCKER_COMPOSE} exec -ti gfar.nginx sh -c '/bin/bash'

docker-shell-cli:
	${DOCKER_COMPOSE} exec -ti gfar.cli sh -c '/bin/bash'

docker-down:
	${DOCKER_COMPOSE} down --rmi all -v --remove-orphans

docker-logs:
	${DOCKER_COMPOSE} logs

docker-restart: docker-stop docker-start

docker-clean:
	- docker stop $(shell docker ps -a -q)
	- docker rm -v $(shell docker ps -a -q)
	- docker volume rm $(shell docker volume ls -q -f dangling=true)
	- docker network rm $(shell docker network ls -q --filter type=custom)
	- docker rmi $(shell docker images -a -q) -f
	- docker builder prune -f
	- docker system prune -a -f


## Deploy commands
###################

define reset_gfar_dir
	rm -rf ${INFRA_GFAR_SRC_PATH}/$(1)
	git checkout HEAD -- ${INFRA_GFAR_SRC_PATH}/$(1)
endef

reset-images:
	$(call reset_gfar_dir,src/resources/products/images)

# Todo : make public directory usage cleaner
reset-public:
	$(call reset_gfar_dir,public)

reset-artifacts: guard-PROJECT_ARTIFACTS_PATH
	- rm -rf ${PROJECT_ARTIFACTS_PATH}
	- mkdir ${PROJECT_ARTIFACTS_PATH}

reset-all: reset-images reset-public reset-artifacts gfar-reset docker-clean
# reset-all: reset-artifacts gfar-reset docker-clean

deploy: reset-artifacts gfar-build guard-INFRA_GFAR_SRC_PATH guard-PROJECT_ARTIFACTS_PATH
	# Necessary for : https://gitlab.com/gitlab-org/gitlab-pages/issues/23
	cp ${INFRA_GFAR_SRC_PATH}/dist/index.html ${INFRA_GFAR_SRC_PATH}/dist/404.html
	mv ${INFRA_GFAR_SRC_PATH}/dist/* ${PROJECT_ARTIFACTS_PATH}
	$(MAKE) gfar-clean-raws

# :point_up: `docker-restart` is required to load ${PROJECT_ARTIFACTS_PATH} to gfar.nginx
# Todo : fix this restart call
local-deploy: reset-artifacts docker-start deploy
