import Vue from 'vue';
import './registerServiceWorker';
import router from './router';

import './plugins/portal-vue';
import './plugins/vue-modal';
import i18n from './plugins/i18n';

import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  router,
  i18n,
  mounted: () => document.dispatchEvent(new Event('x-app-rendered')),
  render: (h) => h(App),
}).$mount('#app');
