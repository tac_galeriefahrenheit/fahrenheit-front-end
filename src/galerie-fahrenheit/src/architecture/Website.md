# Architecture

## Authentification & Administration

Authentification via gitlab.
Si Authentifié sur Gitlab = authentifié sur le site en mode `admin`.
Ce mode `admin` permettra de modifier le contenue en place sur les pages de manière contextuel.

## Pages

> Si internationalisation, base de route par langue (ex: `/fr/ -> HomePage fr`, `/en/ -> HomePage en`)

+ `/` -> [ HomePage ](/pages/HomePage/HomePage)
+ `/createurs` -> [ CreateursPage ](/pages/CreateursPage/CreateursPage)
+ `/galerie` -> [ GaleriePage ](/pages/GaleriePage/GaleriePage)
+ `/archives` -> [ ArchivesPage ](/pages/ArchivesPage/ArchivesPage)
+ `/mentions` -> [ MentionsLegalesPage ](/pages/MentionsLegalesPage/MentionsLegalesPage)
+ `/collection` -> [ CollectionsPage ](/pages/CollectionsPage/CollectionsPage)
  + `/:id_objet` -> [ ProduitPage ](/pages/ProduitPage/ProduitPage)
+ `*` -> [ Error404Page ](/pages/Error404Page/Error404Page)
