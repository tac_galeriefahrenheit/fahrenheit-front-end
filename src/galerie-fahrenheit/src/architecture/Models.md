# Models

This is to represent the various data models manipulated by the application.

## Products

```ts
interface Product {
  id: string;
  name: string;
  family: string;
  description: string;
  authors: Array[string];
  imageList: Array[ProductImage]
  archived: boolean;
  created_at: string;
  updated_at: string;
}
```

## ProductImage

```ts
interface ProductImage {
  id: string;
  name: string;
  // the emplacements where the image is displayed
  emplacements: Array[Emplacement];
  tags: Array[string];
  webUri: string;
  thumbnailUri: string;
}
```

## Emplacement

```ts
interface Emplacement {
  id: string;
  dimension: Dimension;
  passthrough: any;
}
```

## Dimension

```ts
interface Dimension {
  width: int;
  height: int;
  unit: "%" | "px" | "em" | "vw" | "vh";
  passthrough: any;
}
```
