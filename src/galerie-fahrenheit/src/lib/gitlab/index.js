import check from 'micro-check.js';  // eslint-disable-line

const uriRegExp = new RegExp('(http[s]?:\/\/)?[^\s(["<,>]*\.[^\s[",><]*', 'i'); // eslint-disable-line

const DataClientParamsSchema = {
  apiBaseUri: uriRegExp,
  databaseUri: uriRegExp,
};

const DataClientOptionsShema = {
  'headers?': Headers,
};

class DataClient {
  constructor(params, options = {}) {
    check.validate(params, DataClientParamsSchema);
    check.validate(options, DataClientOptionsShema);

    this.apiBaseUri = params.apiBaseUri;
    this.databaseUri = params.databaseUri;

    this._headers = options.headers || new Headers();  // eslint-disable-line
  }

  async request(uri, options) {
    const response = await fetch(uri, {
      headers: this.headers,
      method: 'GET',
      mode: 'cors',
      cache: 'default',
      ...options,
    });
    const json = await response.json();
    if (!response.ok) throw new Error(`FetchError ${response.status} ${response.statusText}${(json && `: ${json.error}`) || ''}`);
    return json;
  }

  async getDatabase(uri = this.databaseUri) {
    const database = await this.request(uri);
    return database;
  }

  get headers() {
    return this._headers;  // eslint-disable-line
  }

  set headers(headers) {
    headers.entries()
      .forEach(([key, value]) => {
        if (this._headers.has(key)) {  // eslint-disable-line
          this._headers.set(key, value); // eslint-disable-line
        } else {
          this._headers.append(key, value); // eslint-disable-line
        }
      });
  }
}

export class GitlabClient extends DataClient {
  constructor(params) {
    super(
      {
        apiBaseUri: params.apiBaseUri,
        databaseUri: `${params.apiBaseUri}${params.projectId}/repository/files/${encodeURIComponent(params.databaseUri)}/raw?ref=${encodeURIComponent(params.commitBranch)}`,
      },
      {
        headers: new Headers({
          'Content-Type': 'application/json',
          ...(params.apiToken && { 'PRIVATE-TOKEN': params.apiToken }),
        }),
      },
    );
    this.apiToken = params.apiToken || null;
    this.projectId = params.projectId;
    this.commitBranch = params.commitBranch;
    this.commitUrl = `${this.apiBaseUri + this.projectId}/repository/commits`;
  }

  // cf. https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions
  async commitActions(actions, commitMessage = 'API commit.') {
    const body = {
      branch: this.commitBranch,
      commit_message: commitMessage,
      actions,
    };

    const options = {
      method: 'POST',
      body: JSON.stringify(body),
    };

    return this.request(this.commitUrl, options);
  }
}

export default GitlabClient;
