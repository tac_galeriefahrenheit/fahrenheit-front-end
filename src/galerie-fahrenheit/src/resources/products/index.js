import check from 'micro-check.js';  // eslint-disable-line

const uuid = require('uuid/v4');


const ProductImageSchema = {
  id: String,
  name: String,
};

export class ProductImage {
  constructor(params) {
    check.validate(params, ProductImageSchema);

    this.id = params.id;
    this.name = params.name;
  }
}

const productDescriptionSchema = {
  fr: String,
  en: String,
};

const productSchema = {
  id: String,
  name: String,
  family: String,
  authors: Array,
  description: productDescriptionSchema,
  imageDisplay: String, // ProductImage id
  imageList: Array,
  // archived: String,
};

export class Product {
  constructor(params) {
    check.validate(params, productSchema);

    this.id = params.id;
    this.name = params.name;
    this.family = params.family;
    this.description = params.description;
    this.authors = params.authors;
    this.imageList = params.imageList.map((i) => new ProductImage(i));
    // TODO : shouldn't we store the real ProductImage ?
    this.imageDisplay = params.imageDisplay;

    this.archived = params.archived === 'archived';
  }

  getDisplayedImage() {
    return this.imageList.filter((i) => i.id === this.imageDisplay)[0];
  }
}


const ProductImageInputSchema = {
  'id?': String,
  'name?': String,
  'content?': String,
};

export class ProductImageInput {
  constructor(params) {
    check.validate(params, ProductImageInputSchema);

    this.id = params.id;
    this.name = params.name;
    this.content = params.content;
    this.lastForgedProductImage = null;
  }

  forgeProductImage() {
    this.lastForgedProductImage = new ProductImage({
      id: this.id || uuid(),
      name: this.name,
    });

    return this.lastForgedProductImage;
  }
}


const productInputSchema = {
  'id?': String,
  name: String,
  family: String,
  description: productDescriptionSchema,
  'authors?': Array,
  'imageList?': Array,
  'archived?': Boolean,
};

export class ProductInput {
  constructor(params) {
    check.validate(params, productInputSchema);

    this.id = params.id;
    this.name = params.name;
    this.family = params.family;
    this.description = params.description;
    this.authors = params.authors || [];
    this.imageList = params.imageList || [];
    this.archived = params.archived || false;

    this.lastForgedProduct = null;
  }

  loadImageFile(imageFile) {
    return new Promise((resolve) => {
      const reader = new FileReader();
      const self = this;

      reader.onload = function onload() {
        // https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
        const result = reader.result.replace(`data:${imageFile.type};base64,`, '');

        self.imageList.push(new ProductImageInput({
          content: result,
          name: imageFile.name,
        }));
        resolve(self);
      };

      reader.readAsDataURL(imageFile);
    });
  }

  forgeProduct() {
    const imageList = this.imageList.map((imageInput) => imageInput.forgeProductImage());
    this.lastForgedProduct = new Product({
      id: this.id || uuid(),
      name: this.name,
      family: this.family,
      authors: this.authors.map(({ id }) => id),
      description: this.description,
      imageDisplay: imageList[0].id,
      imageList,
      archived: this.archived,
    });

    return this.lastForgedProduct;
  }
}
