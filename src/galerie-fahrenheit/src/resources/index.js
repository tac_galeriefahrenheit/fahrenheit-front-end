import gfData from './data.json';
import { Product } from './products';

const uuid = require('uuid/v4');

export class DataService {
  constructor(clientData = gfData, dataClient) {
    this.imagesBasePath = 'src/galerie-fahrenheit/src/resources/products/images/';
    this.dbPath = 'src/galerie-fahrenheit/src/resources/data.json';
    this.baseUri = process.env.NODE_ENV === 'production'
      ? 'https://galerie-fahrenheit.com/assets/images/products'
      : '/assets/images/products';
    this.srcImagePrefix = 'raw.';

    this._dataClient = dataClient;  // eslint-disable-line
    this._clientData = clientData; // eslint-disable-line
  }

  get dataClient() {
    return this._dataClient ? this._dataClient : undefined; // eslint-disable-line
  }

  set dataClient(client) {
    this._dataClient = client; // eslint-disable-line
  }

  get products() {
    return this.clientData.products.map((p) => new Product(p)).reverse();
  }

  get clientData() {
    return this._clientData; // eslint-disable-line
  }

  set clientData(data) {
    this._clientData = data; // eslint-disable-line
  }

  /** *****************************************
   ** Should be private
   ****************************************** */

  // return true if given product match with filter
  applyFilter(product, filter) { // eslint-disable-line
    if (filter == null) {
      return true;
    }
    const idFilter = filter.id == null || filter.id === product.id;
    const familyFilter = filter.family == null || filter.family === product.family;
    const imageFilter = filter.imageId == null
      || (product.imageList && product.imageList
        .filter((i) => i.id === filter.imageId).length === 1);
    return idFilter && familyFilter && imageFilter;
  }

  forgeImageFilePath(productId, imageId, imageName) {
    const basePath = `${this.imagesBasePath}${productId}/${imageId}/`;

    if (imageName.startsWith(this.srcImagePrefix)) {
      return basePath + imageName;
    }
    return basePath + this.srcImagePrefix + imageName;
  }

  /** *****************************************
   ** Products
   ****************************************** */

  generateProduct(values) { // eslint-disable-line
    return new Product({
      id: uuid(),
      name: values.name,
      family: values.family,
      authors: values.authors,
    });
  }

  getProducts(filter) {
    return this.products.filter((p) => this.applyFilter(p, filter));
  }

  async saveProduct(productInput) {
    if (productInput.id == null) {
      // add new product
      const newProduct = productInput.forgeProduct();
      this.clientData.products.push(newProduct);

      const gitActions = [
        {
          action: 'update',
          file_path: this.dbPath,
          content: JSON.stringify(this.clientData),
        },
      ];

      productInput.imageList.forEach((imageInput) => {
        gitActions.push({
          action: 'create',
          file_path: this.forgeImageFilePath(
            newProduct.id,
            imageInput.lastForgedProductImage.id,
            imageInput.name,
          ),
          content: imageInput.content,
          encoding: 'base64',
        });
      });

      return this.dataClient.commitActions(gitActions, 'Add a product.');
    }
    // edit existing product
    throw new Error('TODO : edit existing product');
  }

  deleteProduct(filter) {
    const filteredProducts = this.products.filter((p) => !this.applyFilter(p, filter));
    const productsToDelete = this.products.filter((p) => this.applyFilter(p, filter));

    this.clientData.products = filteredProducts;
    // delete product from home page if present
    this.clientData.pages.home.product_list = this.clientData.pages.home.product_list
      .filter((id) => !this.applyFilter({ id }, filter));
    const gitActions = [
      {
        action: 'update',
        file_path: this.dbPath,
        content: JSON.stringify(this.clientData),
      },
    ];

    productsToDelete.forEach((p) => p.imageList.forEach((i) => {
      gitActions.push({
        action: 'delete',
        file_path: this.forgeImageFilePath(p.id, i.id, i.name),
      });
    }));

    return this.dataClient.commitActions(gitActions, 'Delete product(s).');
  }

  // TODO : we assume that we don't have to manage image update.
  saveSiteState(commitMessage) {
    const gitActions = [
      {
        action: 'update',
        file_path: this.dbPath,
        content: JSON.stringify(this.clientData),
      },
    ];
    return this.dataClient.commitActions(gitActions, commitMessage);
  }

  /** *****************************************
   ** Images
   ****************************************** */

  // TODO : add byId filter
  getImages(filter) {
    if (filter == null) {
      return this.getProducts().reduce((acc, product) => acc.concat(product.imageList), []);
    }
    const products = filter.product == null ? this.getProducts() : this.getProducts(filter.product);
    return products.reduce((acc, product) => acc.concat(product.imageList), []);
  }

  getWebUri(image, product) {
    return `${this.baseUri}/${product.id}/${image.id}/web.jpg`;
  }

  getThumbnailUri(image, product) {
    return `${this.baseUri}/${product.id}/${image.id}/thumbnail.jpg`;
  }

  /** *****************************************
   ** Authors
   ****************************************** */

  /**
   * If no authorId given, return all authors
   */
  getAuthors(authorId) {
    if (authorId == null) {
      return this.clientData.authors;
    }
    return this.clientData.authors.filter((a) => a.id === authorId);
  }

  /** *****************************************
   ** Pages
   ****************************************** */

  /**
   * TODO : make a better pages content management system.
   */
  getHomePage() {
    return {
      products: this.clientData.pages.home.product_list
        .map((pId) => this.getProducts({ id: pId })[0]),
    };
  }

  addProductToHomePage(productId) {
    this.clientData.pages.home.product_list.push(productId);
  }

  removeProductToHomePage(productId) {
    this.clientData.pages.home.product_list = this.clientData.pages.home.product_list.filter(
      (pid) => pid !== productId,
    );
  }
}

export default new DataService();
