import Vue from 'vue';
import VueI18n from 'vue-i18n';

import en from '../../resources/translations/en';
import fr from '../../resources/translations/fr';

Vue.use(VueI18n);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'fr', // set locale
  messages: {
    fr,
    en,
  }, // set locale messages
});

export default i18n;
