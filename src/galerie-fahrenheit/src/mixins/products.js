import { ProductInput } from '../resources/products';

export const productByFamily = (family) => {
  if (family == null) {
    throw new Error('no family given');
  }

  const productFilter = {
    family,
  };

  return {
    computed: {
      products() {
        return this.dataService.getProducts(productFilter);
      },
      images() {
        const filter = {
          product: productFilter,
        };
        return this.dataService.getImages(filter);
      },
    },
  };
};

export const dataHandling = () => ({
  methods: {
    getAuthor(id) {
      return this.dataService.getAuthors(id)[0];
    },
    getWebUri(image, product) {
      return this.dataService.getWebUri(image, product);
    },
  },
});

export const editProduct = () => ({
  methods: {
    getToken() {
      return window.prompt('Gitlab token please:');  // eslint-disable-line
    },
    defaultProductInput() {
      return new ProductInput({
        name: 'Default product',
        family: 'objet',
        description: {
          fr: 'description en francais',
          en: 'english description',
        },
      });
    },
    updateProduct(productInput) {
      return this.dataService.saveProduct(productInput);
    },
    deleteProduct(productId) {
      const filter = {
        id: productId,
      };
      return this.dataService.deleteProduct(filter);
    },
    loadImagesToProductInput(files, productInput) {
      return Promise.all(
        Array.from(files).map((imageFile) => productInput.loadImageFile(imageFile)),
      );
    },
  },
});

export default () => ({
  data() {
    return {
      cache_authors: [],
      cache_products: [],
      cache_images: [],
    };
  },
  methods: {
    async refreshDatabase() {
      if (this.dataService.dataClient.getDatabase) {
        const data = await this.dataService.dataClient.getDatabase();
        this.dataService.clientData = data;
        this.products = '';
        this.authors = '';
      }
    },
  },
  computed: {
    authors: {
      get() {
        return this.cache_authors;
      },
      set() {
        this.cache_authors = this.dataService.getAuthors();
      },
    },
    products: {
      get() {
        return this.cache_products;
      },
      set() {
        this.cache_products = this.dataService.getProducts();
      },
    },
    images: {
      get() {
        return this.cache_images;
      },
      set() {
        this.cache_images = this.dataService.getImages();
      },
    },
  },
});
