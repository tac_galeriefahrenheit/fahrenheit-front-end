import Vue from 'vue';
import Router from 'vue-router';

import HeaderBar from '../components/HeaderBar/index.vue';
import FooterBar from '../components/FooterBar/index.vue';
import HomePage from '../pages/HomePage.vue';

const ArchivesPage = () => import(/* webpackChunkName: "ArchivesPage" */ '../pages/ArchivesPage.vue');
const CollectionPage = () => import(/* webpackChunkName: "CollectionPage" */ '../pages/CollectionPage.vue');
const CreateursPage = () => import(/* webpackChunkName: "CreateursPage" */ '../pages/CreateursPage.vue');
const Error404Page = () => import(/* webpackChunkName: "Error404Page" */ '../pages/Error404Page.vue');
const GaleriePage = () => import(/* webpackChunkName: "GaleriePage" */ '../pages/GaleriePage.vue');
const ProduitPage = () => import(/* webpackChunkName: "ProduitPage" */ '../pages/ProduitPage.vue');
const LegalPage = () => import(/* webpackChunkName: "LegalPage" */ '../pages/LegalPage.vue');
const AdminPage = () => import(/* webpackChunkName: "AdminPage" */ '../pages/AdminPage.vue');

Vue.use(Router);

const routes = [
  {
    path: '/',
    name: 'homepage',
    components: {
      default: HomePage,
      header: HeaderBar,
      footer: FooterBar,
    },
    props: {
      header: {
        black: window.innerWidth < 635,
      },
    },
  },
  {
    path: '/createurs',
    name: 'createurs',
    components: {
      default: CreateursPage,
      header: HeaderBar,
      footer: FooterBar,
    },
    props: {
      header: {
        black: true,
      },
    },
  },
  {
    path: '/galerie',
    name: 'galerie',
    components: {
      default: GaleriePage,
      header: HeaderBar,
      footer: FooterBar,
    },
    props: {
      header: {
        black: window.innerWidth < 635,
      },
    },
  },
  {
    path: '/archives',
    name: 'archives',
    components: {
      default: ArchivesPage,
      header: HeaderBar,
      footer: FooterBar,
    },
  },
  {
    path: '/collection',
    name: 'collection',
    components: {
      default: CollectionPage,
      header: HeaderBar,
      footer: FooterBar,
    },
    props: {
      header: {
        black: true,
      },
    },
  },
  {
    path: '/produits/:product_id',
    components: {
      default: ProduitPage,
      header: HeaderBar,
      footer: FooterBar,
    },
    props: {
      header: {
        black: true,
      },
    },
  },
  {
    path: '/admin',
    name: 'admin',
    components: {
      default: AdminPage,
      header: HeaderBar,
      footer: FooterBar,
    },
    props: {
      header: {
        black: true,
      },
    },
  },
  {
    path: '/legal',
    name: 'legal',
    components: {
      default: LegalPage,
      header: HeaderBar,
      footer: FooterBar,
    },
    props: {
      header: {
        black: true,
      },
    },
  },
  {
    path: '/explore',
    redirect: '/',
  },
  {
    path: '/en/explore',
    redirect: '/',
  },
  {
    path: '/fr/explore',
    redirect: '/',
  },
  {
    path: '*',
    name: '404',
    components: {
      default: Error404Page,
      header: HeaderBar,
      footer: FooterBar,
    },
  },
];

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
