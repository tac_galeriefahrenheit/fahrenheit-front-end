module.exports = {
  pwa: {
    name: 'Galerie-Fahrenheit',
    iconPaths: {
      faviconSVG: null,
      favicon32: null,
      favicon16: null,
      appleTouchIcon: null,
      maskIcon: null,
      msTileImage: null,
    },
    // https://github.com/vuejs/vue-cli/issues/4069
    manifestOptions: {
      icons: null
    }
  },

  pluginOptions: {
    prerenderSpa: {
      registry: undefined,
      renderRoutes: [
        '/',
        '/createurs',
        '/galerie',
        '/collection',
        '/legal',
      ],
      useRenderEvent: true,
      headless: true,
      onlyProduction: true,
      postProcess: (route) => {
        // Defer scripts and tell Vue it's been server rendered to trigger hydration
        route.html = route.html // eslint-disable-line
          .replace(/<script (.*?)>/g, '<script $1 defer>')
          .replace('id="app"', 'id="app" data-server-rendered="true"');
        return route;
      },
    },
  },

  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].title = 'Galerie Fahrenheit'; // eslint-disable-line
        return args;
      });
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/'
    : './',
};
